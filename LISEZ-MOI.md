# BetterJoyForCemu - modifié pour le projet SteamBall sur Unreal Engine 4.
Le projet [BetterJoy](https://github.com/Davidobot/BetterJoy) a été modifié pour récuperer les valeurs des gyromètres et des acceléromètres des Joy-Cons à la place 
des valeurs des axes (gachettes et joysticks) et être interpréter comme manette de Xbox sur windows 10.
--Modifié par Bruno FACHE pour un projet Educatif

# BetterJoy (version SteamBall)
Permet aux Joy-Cons d'être utilisé avec win64 en émulant un comportement de controller de Xbox gérer nativement par windows.

## Executables (version SteamBall)
Télécharger ic l'executable pour win64.
* [64-bit](https://ludusacademie-my.sharepoint.com/:u:/g/personal/b_fache_ludus-academie_com/EZlOYewo8p5IiyAt2tEuce4BmfPxV7gaTrTgopKThqxq6g?e=Hru8bV)

### Capture d'écran
Minimisé             |  Etendue
:-------------------------:|:-------------------------:
![Example](https://user-images.githubusercontent.com/16619943/67919451-bf8e5680-fb76-11e9-995e-7193b87548e1.png)|![Example](./Examples/example3.png)

# Instructions d'installation
1. Installer les pilotes
    1. Exécuter *! Driver Install (Run as Admin).bat* en tant qu'administrateur.
2. Exécuter BetterJoyForCemu.exe
    1. Si vous l'exécuter pour la première fois, il peut y avoir des bug - fermer BetterJoy et redémarrer votre ordianter pour que les pilotes s'installent correctement.
    2. Sinon, consulter la section __Problems__ du fichier Readme.md (en anglais) .
3. Connecter les Joy-Cons en bluetooth.
4. Utiliser les deux controlleurs en mode combiné pour émuler un seul controlleur Xbox. 
    * (Cliquer sur l'image d'un des contôlleurs, ils doivent être à la verticale quand ils sont correctment combiné)

## Note: Pour SteamBall:
* Controller Steamball avec des Joy-Cons est une fonction expérimentale. Préférez utiliser des controlleurs VR classique.
* Le bluetooth est nécessaire sur l'odrinateur pour pouvoir connecter les Joy-Cons.
* Ne modifier pas la configuration initiale dans *BetterJoyForCemu.exe.config* ou en changeant les paramètres dans l'application.
* Ne connecter aucun autre controlleur Xbox. Si c'est le cas, déconnecter les et redémarrer votre ordinateur.

_Pour plus d'information veuillez consulter le fichier Readme.md (en anglais)._

# Remerciements
Nous remercions chaleuresement [Davidobot](https://github.com/Davidobot) pour son travail sur BetterJoy ainsi que tous ceux qui l'ont aidé.
