# BetterJoyForCemu - modified for SteamBall project on Unreal Engine 4.
The project [BetterJoy](https://github.com/Davidobot/BetterJoy) was modified in order to replace axes values by gyrometers and accelerometes input values when interpret as XBoxController.
--Modified by Bruno FACHE for an Educational Project

*Une traduction est disponible dans le fichier [LISEZ-MOI.md](https://gitlab.com/steamball/BetterJoy/-/blob/GyroOverTiggerInputs-ProjectForUnrealEngine/LISEZ-MOI.md).*

# BetterJoy (version SteamBall)
Allows the Joy-Cons to be used with win64 by using a generic XInput support.

## Builds (version SteamBall)
Here to download the executable for win64.
* [64-bit](https://ludusacademie-my.sharepoint.com/:u:/g/personal/b_fache_ludus-academie_com/EZlOYewo8p5IiyAt2tEuce4BmfPxV7gaTrTgopKThqxq6g?e=Hru8bV)

### Screenshots
Collapsed             |  Expanded
:-------------------------:|:-------------------------:
![Example](https://user-images.githubusercontent.com/16619943/67919451-bf8e5680-fb76-11e9-995e-7193b87548e1.png)|![Example](./Examples/example3.png)

# How to use
1. Install drivers
    1. Run *! Driver Install (Run as Admin).bat*
2. Run BetterJoyForCemu.exe
    1. If running for the first time, things might glitch out - just close BetterJoyForCemu normally and restart your computer for the drivers to take effect.
    2. If not, see the __Problems__ section.
3. Connect your controllers.
4. Use the both controllers in dual mode in order to emulate only one Xbox360 controller.

## Note: For SteamBall:
* Controlling SteamBall with Joy-Cons is experimental prefer use classic VR controllers.
* You need bluetooth on your computer in order to be able to connect the Joy-Cons
* Do not change initial configuration in *BetterJoyForCemu.exe.config* or by changing setting in the app.
* Do not connect any other Xbox360 controller to your computer. If it is the case disconnect them and reboot your computer.

# App Settings
Feel free to edit *BetterJoyForCemu.exe.config* before running the program to configure it to your liking.

Current settings are:
* IP address of CemuHook motion server  *(default: 127.0.0.1)*
* Port number of CemuHook motion server *(default: 26760)*
* Rumble Period of motor in ms          *(default: 300)*
* Frequency of low rumble in Hz         *(default: 20)*
* Frequency of high rumble in Hz        *(default: 400)*
* Rumble - en/disables rumble           *(default: true)*
* Swap A-B                              *(default: false)*
  * Swaps the A-B buttons to mimick the Xbox layout by button name, rather than physical layout 
* Swap X-Y                              *(default: false)*
  * Swaps the X-Y buttons to mimick the Xbox layout by button name, rather than physical layout 
* PurgeWhitelist                        *(default: true)*
  * Determines whether or not HidGuardian's process whitelist is purged on start-up
* UseHIDG                               *(default: true)*
  * Determines whether or not to use HidGuardian (improves compatibility with other programs, like Steam, when set to "false")
* ShowAsXInput                          *(default: true)*
  * Determines whether or not the program will expose detected controllers as Xbox 360 controllers
* PurgeAffectedDevices                  *(default: true)*
  * Determines whether or not the program should purge the affected devices list upon exit
  * Should prevent any more issues of the controller being unusable after the program
* NonOriginalController                 *(default: false)*
  * When "true", click the "Calibrate" button once to calibrate the gyroscope on your connect pro controller
* HomeLEDOn" 
   * Default value="false". Control the light of the HomeLedButton.
* GyroOverSticks 
   * Default value="true". Replace axes input values by Gyrometers inputs values. (ShowAsXInput need to be enable in order to work)
* AccelOverSticks"
   * Default value="false". Replace axes input values by Gyrometers inputs values. (ShowAsXInput need to be enable in order to work)

## Note:
__You can use both GyroOverSticks and AccelOverSticks int the same time but only if you are using joycon in sigle mode, otherwisee, GyroOverSticks will override AccelOverSticks__

# Problems
__Make sure you installed the drivers!!__

__Controller is not recognised after using the program__

Before uninstalling the drivers, navigate to http://localhost:26762/ and remove all the devices from the "Currently affected devices" list and then restart your computer.

__Make pro controller or Joycons visible to other programs again without uninstalled HidGuardian__

BetterJoyForCemu automatically adds Joycons and Pro Controllers to HidGuardian's blacklist upon start-up.

However, to manually remove the devices from the blacklist, one can navigate to this page: http://localhost:26762/

__Calibration Issues (ex: sticks don't have full range)__

Switch off "also use for axes/buttons" under motion settings and set the input deadzones to 0.

__Motion controls don't work/work badly__

While the program is running, turn off your controller (if USB - unplug, if BT - press the sync button) and then turn it back on (press any button).

__No Joycons detected__

If using Bluetooth - see the "How to properly disconnect the controller" section and follow the steps listed there. Then, reconnect the controller.

If using USB - try unplugging the controller and then plugging it back in, making sure to let Windows set it up before launching the program.

__Getting stuck at "Using USB" or "Using factory.."__

Close the program and then start it again. If it doesn't work, see the "No joycons detected" section and try that.

__CemuHook not recognising the controller__

Make sure that CemuHook settings are at their default state, which are -

```
serverIP = 127.0.0.1
serverPort = 26760
```

__Plugging controller into USB port does nothing__

Solution found courtresy of reddit user BFCE -  go into Device Manager, go to the Universal Serial Bus Controllers, select the properties of the eXtreme (or other USB) controllers, and toggle the setting that allows you to disable the USB ports to save power when not in use. Even some desktops have this on by default.

___Note that for Joycons to work properly, you need a decent Bluetooth adapter that is comfortable with handling 3/4 connections at a time.___

Feel free to open a new issue if you have any comments or questions.

# Connecting and Disconnecting the Controller
## Bluetooth Mode
Hold down the small button on the top of the controller for 5 seconds - this puts the controller into broadcasting mode.

Search for it in your bluetooth settings and pair normally.

To disconnect the controller - press down the button once. To reconnect - press any button on your controller.

## How to properly disconnect the controller
### Windows 10
1. Go into "Bluetooth and other devices settings"
1. Under the first category "Mouse, keyboard, & pen", there should be the pro controller.
1. Click on it and a "Remove" button will be revealed.
1. Press the "Remove" button

# Acknowledgements
A massive thanks goes out to [Davidobot](https://github.com/Davidobot) for his work on BetterJoy and for all the people who helped him.
